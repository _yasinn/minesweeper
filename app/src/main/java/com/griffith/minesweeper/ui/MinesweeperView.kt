package com.griffith.minesweeper.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.griffith.minesweeper.MainActivity
import com.griffith.minesweeper.R
import com.griffith.minesweeper.model.MinesweeperModel

class MinesweeperView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    companion object {
        private const val DEFAULT_BOARD_COVERED_COLOR = Color.BLACK
        private const val DEFAULT_BOARD_UNCOVERED_COLOR = Color.GRAY
        private const val DEFAULT_BOARD_LINE_COLOR = Color.WHITE
        private const val DEFAULT_BOARD_LINE_STROKE_WIDTH = 10F
        private const val DEFAULT_BOARD_TEXT = Color.WHITE
        private const val DEFAULT_BOARD_TEXT_SIZE = 90F
        private const val DEFAULT_UNCOVERED_MINE_COLOR = Color.RED
        private const val DEFAULT_UNCOVERED_MINE_TEXT_COLOR = Color.BLACK
        private const val DEFAULT_UNCOVERED_MINE_TEXT_SIZE = 90F
        private const val DEFAULT_UNCOVERED_FLAG_TEXT_SIZE = 90F
        private val DEFAULT_BOARD_LINE_STYLE = Paint.Style.STROKE
    }

    private val paintCoveredBoard = Paint()
    private val paintLine = Paint()
    private val paintTextWhite = Paint()
    private val paintUncoveredBoard = Paint()
    private val paintUncoveredMineText = Paint()
    private val paintUncoveredFlagText = Paint()
    private val paintUncoveredMineBoard = Paint()

    init {
        setupAttributes(attrs)
    }

    private fun setupAttributes(attrs: AttributeSet?){
        // covered game area
        paintCoveredBoard.color = DEFAULT_BOARD_COVERED_COLOR

        // uncovered game area
        paintUncoveredBoard.color = DEFAULT_BOARD_UNCOVERED_COLOR

        //white lines defining game area
        paintLine.color = DEFAULT_BOARD_LINE_COLOR
        paintLine.strokeWidth = DEFAULT_BOARD_LINE_STROKE_WIDTH
        paintLine.style = DEFAULT_BOARD_LINE_STYLE

        //white text
        paintTextWhite.color = DEFAULT_BOARD_TEXT
        paintTextWhite.textSize = DEFAULT_BOARD_TEXT_SIZE

        //mine text
        paintUncoveredMineText.color = DEFAULT_UNCOVERED_MINE_TEXT_COLOR
        paintUncoveredMineText.textSize = DEFAULT_UNCOVERED_MINE_TEXT_SIZE

        // uncovered mine board
        paintUncoveredMineBoard.color = DEFAULT_UNCOVERED_MINE_COLOR

        // flag
        paintUncoveredFlagText.color = Color.parseColor("#FFF57C00")
        paintUncoveredFlagText.textSize = DEFAULT_UNCOVERED_FLAG_TEXT_SIZE
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        paintTextWhite.textSize = height.toFloat() / MinesweeperModel.numRows
        paintUncoveredFlagText.textSize =  height.toFloat() / MinesweeperModel.numRows
        paintUncoveredMineText.textSize = height.toFloat() / MinesweeperModel.numRows
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paintCoveredBoard)
        drawGameArea(canvas)
        drawAllSymbols(canvas)
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paintLine)
    }

    private fun drawGameArea(canvas: Canvas) {
        //draw horizontal lines
        for (i in 1 until MinesweeperModel.numRows) {
            canvas.drawLine(0f, (i * height / MinesweeperModel.numRows).toFloat(), width.toFloat(), (i * height / MinesweeperModel.numRows).toFloat(),
                paintLine)
        }
        //draw vertical lines
        for (i in 1 until MinesweeperModel.numColumns) {
            canvas.drawLine((i * width / MinesweeperModel.numColumns).toFloat(), 0f, (i * width / MinesweeperModel.numColumns).toFloat(), height.toFloat(),
                paintLine)
        }
    }

    //draw numbers and symbols in the fields of the MinesweeperView
    private fun drawAllSymbols(canvas: Canvas) {
        for (i in 0 until MinesweeperModel.numColumns) {
            for (j in 0 until MinesweeperModel.numRows) {
                drawSymbol(i, j, canvas)
            }
        }
    }

    //draw a single symbol in a given field of the MinesweeperView based on its content in the model
    private fun drawSymbol(i: Int, j: Int, canvas: Canvas) {
        val squareWidth = width.toFloat() / MinesweeperModel.numColumns
        val squareHeight = height.toFloat() / MinesweeperModel.numRows
        //if FLAGGED, draw flag symbol
        if (MinesweeperModel.getCoverState(i, j) == MinesweeperModel.FLAGGED) {
            //add flag symbol
            canvas.drawText("⚐", i * squareWidth + (.13 * squareWidth).toFloat(), (j + 1) * squareHeight - (.15 * squareHeight).toFloat(), paintUncoveredFlagText)
        }
        //if UNCOVERED, draw mine or mine counter number
        if (MinesweeperModel.getCoverState(i, j) == MinesweeperModel.UNCOVERED) {
            //gray out the background
            canvas.drawRect(i * squareWidth, j * squareHeight, (i + 1) * squareWidth, (j + 1) * squareHeight, paintUncoveredBoard)
            if (MinesweeperModel.isMine(i, j)) {
                //add mine symbol
                canvas.drawRect(i * squareWidth, j * squareHeight, (i + 1) * squareWidth, (j + 1) * squareHeight, paintUncoveredMineBoard)
                canvas.drawText("M", i * squareWidth + (.12 * squareWidth).toFloat(), (j + 1) * squareHeight - (.13 * squareHeight).toFloat(), paintUncoveredMineText)
            } else {
                var counter = MinesweeperModel.getMineCounter(i, j)
                if (counter != 0.toShort()) {
                    //add number of mines nearby
                    canvas.drawText(counter.toString(), i * squareWidth + (.22 * squareWidth).toFloat(), (j + 1) * squareHeight - (.14 * squareHeight).toFloat(), paintTextWhite)
                }
            }
        }
    }

    //update the view and model based on the conditions of the game at the time and location of the touch event
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val tX = event.x.toInt() / (width / MinesweeperModel.numColumns)
        val tY = event.y.toInt() / (height / MinesweeperModel.numRows)

        //if the game is not over, change the state of the field at (tX, tY) in the model
        if (!MinesweeperModel.gameOver && !MinesweeperModel.isWin && (tX < MinesweeperModel.numColumns) && (tY < MinesweeperModel.numRows)) {
            //if no flags are left, display snackbar message
            if (MinesweeperModel.mode == MinesweeperModel.FLAG &&
                MinesweeperModel.getCoverState(tX, tY) == MinesweeperModel.COVERED &&
                MinesweeperModel.numFlags == 0)
                    (context as MainActivity).displaySnackbar(context.getString(R.string.no_flags))
            MinesweeperModel.changeSquareState(tX, tY)
            (context as MainActivity).showFlagNumber(MinesweeperModel.numFlags)
        }
        if (MinesweeperModel.gameOver) {
            (context as MainActivity).displaySnackbar(context.getString(R.string.game_over))
            (context as MainActivity).visibilityCountdown(false)
        }

        if (MinesweeperModel.isWin) (context as MainActivity).displaySnackbar(context.getString(R.string.you_win))

        invalidate()

        return super.onTouchEvent(event)
    }

    // reset the board
    fun restart() {
        MinesweeperModel.resetModel()
        (context as MainActivity).showFlagNumber(MinesweeperModel.numFlags)
        invalidate()
    }

}