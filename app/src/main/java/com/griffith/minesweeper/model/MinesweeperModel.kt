package com.griffith.minesweeper.model

import java.util.*

object MinesweeperModel {

    // the Point class contains info about a specific field in the model:
    // x = whether that field is a MINE and if not, how many mines are around it
    // y = cover state of the field
    data class Point(var x : Short, var y : Short)

    // 10x10 board
    const val numRows = 10
    const val numColumns = 10

    // mine pointer
    private const val MINE: Short = -1

    //cover states
    const val COVERED: Short = -2
    const val UNCOVERED: Short = -3
    const val FLAGGED: Short = -4

    //modes
    const val TRY: Short = -5
    const val FLAG: Short = -6

    // number of mines
    private const val numMines = 20
    private const val defaultNumFlags = numMines

    // game state
    var gameOver = false
    var isWin = false

    // default mode
    var mode = TRY
    var numFlags = defaultNumFlags

    private var model = generateRandomSetup()

    //randomly place the mines on the board, then update the number of mines near each field
    private fun generateRandomSetup(): Array<Array<Point>> {
        val array = Array(numColumns) { Array(numRows) {Point(0,COVERED)}}

        placeMines(array)

        //calculate numbers for each field based on its proximity to the mines
        for (i in 0 until numColumns) {
            for (j in 0 until numRows) {
                if (array[i][j].x != MINE) {
                    //set the mine counter for the current square
                    array[i][j].x = countMinesNearSquare(array, i, j).toShort()
                }
            }
        }
        return array
    }

    private fun countMinesNearSquare(array: Array<Array<Point>>, i: Int, j: Int): Int {
        var counter = 0
        if (i-1>=0 && j-1>=0 && array[i-1][j-1].x == MINE) counter +=1
        if (i-1>=0 && array[i-1][j].x == MINE) counter += 1
        if (i-1>=0 && j+1<numRows && array[i-1][j+1].x == MINE) counter +=1
        if (j-1>=0 && array[i][j-1].x == MINE) counter += 1
        if (j+1<numRows && array[i][j+1].x == MINE) counter += 1
        if (i+1<numColumns && j-1>=0 && array[i+1][j-1].x == MINE) counter += 1
        if (i+1<numColumns && array[i+1][j].x == MINE) counter += 1
        if (i+1<numColumns && j+1<numRows && array[i+1][j+1].x == MINE) counter +=1
        return counter
    }

    //place mines randomly on the board by generating pairs of random numbers
    private fun placeMines(array: Array<Array<Point>>) {
        val random = Random()
        for (i in 1..numMines) {
            var mineLocation = Pair(random.nextInt(numColumns), random.nextInt(numRows))
            //check that there isn't already a mine at this spot
            while (array[mineLocation.first][mineLocation.second].x == MINE) {
                mineLocation = Pair(random.nextInt(numColumns), random.nextInt(numRows))
            }
            array[mineLocation.first][mineLocation.second].x = MINE
        }
    }

    fun getCoverState(x: Int, y: Int): Short = model[x][y].y

    // check related square is mine?
    fun isMine(x: Int, y: Int): Boolean {
        var truth = false
        if (model[x][y].x == MINE) {
            truth = true
        }
        return truth
    }

    //returns the number of mines near a given square
    fun getMineCounter(x: Int, y: Int): Short = model[x][y].x

    //change the cover state at the field (x, y)
    private fun setCoverState(x: Int, y: Int, state: Short) {
        model[x][y].y = state

        numFlags = when {
            // when the action is flagging, decrease the number of available flags
            (state == FLAGGED) -> numFlags - 1
            // when the action is un-flagging, increase the number of available flags
            (state == COVERED) -> numFlags + 1
            else -> numFlags
        }

        isGameOver()
        isForWin()
        if (gameOver) {
            uncoverAllMines()
        }
    }

    // uncover all the mines on the board
    private fun uncoverAllMines() {
        for (i in 0 until numColumns) {
            for (j in 0 until numRows) {
                if (model[i][j].x == MINE) {
                    model[i][j].y = UNCOVERED
                }
            }
        }
    }

    // change the state of the field at (tX, tY) depending on whether in TRY or FLAG mode
    fun changeSquareState(tX: Int, tY: Int) {
        when (mode) {
            TRY -> {
                //if in TRY mode, uncover the square
                setCoverState(tX, tY, UNCOVERED)
                //if the square is empty (i.e. no mines nearby), uncover all squares around it
                if ((model[tX][tY].x).toInt() == 0) uncoverAllSquaresAround(tX, tY)
            }
            FLAG -> {
                //when in FLAG mode, toggle between FLAGGED and COVERED depending on
                // the current cover state of the square
                when (getCoverState(tX, tY)) {
                    FLAGGED -> setCoverState(tX, tY, COVERED)
                    COVERED -> {
                        //if player is not already out of flags, then they can flag the square
                        if (numFlags != 0) setCoverState(tX, tY, FLAGGED)
                    }
                }
            }
        }
    }

    private fun uncoverAllSquaresAround(tX: Int, tY: Int) {
        if (tX-1>=0 && tY-1>=0 && (model[tX-1][tY-1].y) != UNCOVERED) changeSquareState(tX - 1, tY - 1)
        if (tY-1>=0 && (model[tX][tY-1].y) != UNCOVERED) changeSquareState(tX, tY - 1)
        if (tX+1<numColumns && tY-1>=0 && (model[tX+1][tY-1].y) != UNCOVERED) changeSquareState(tX + 1, tY - 1)
        if (tX-1>=0 && (model[tX-1][tY].y) != UNCOVERED) changeSquareState(tX - 1, tY)
        if (tX+1<numColumns && (model[tX+1][tY].y) != UNCOVERED) changeSquareState(tX + 1, tY)
        if (tX-1>=0 && tY+1<numRows && (model[tX-1][tY+1].y) != UNCOVERED) changeSquareState(tX - 1, tY + 1)
        if (tY+1<numRows && (model[tX][tY+1].y) != UNCOVERED) changeSquareState(tX, tY + 1)
        if (tX+1<numColumns && tY+1<numRows && (model[tX+1][tY+1].y) != UNCOVERED) changeSquareState(tX + 1, tY + 1)
    }

    //if any mine is uncovered, then the game is over
    private fun isGameOver() {
        for (row in model) {
            for (square in row) {
                if (square.x == MINE && square.y == UNCOVERED) {
                    gameOver = true
                }
            }
        }
    }

    /**
     * check if player has won by making sure all mines are still covered
     * (or flagged) and all non-mine squares are uncovered
     * */
    private fun isForWin() {
        var mineCounter = 0
        var nonMineCounter = 0
        for (row in model) {
            for (square in row) {
                if ((square.x == MINE && square.y == COVERED) || (square.x == MINE && square.y == FLAGGED)) {
                    mineCounter +=1
                }
                if (square.x != MINE && square.y == UNCOVERED) {
                    nonMineCounter +=1
                }
            }
        }
        //if all non-mine squares are uncovered and all mines are either covered or flagged, player wins
        if (mineCounter == numMines && nonMineCounter == ((numRows * numColumns) - numMines)) {
            isWin = true
        }
    }

    //restore all default values
    fun resetModel() {
        gameOver = false
        isWin = false
        mode = TRY
        numFlags = defaultNumFlags

        //generate a new random setup
        model = generateRandomSetup()
    }

}