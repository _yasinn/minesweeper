package com.griffith.minesweeper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.griffith.minesweeper.databinding.ActivityMainBinding
import com.griffith.minesweeper.model.MinesweeperModel
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    private lateinit var _ui: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _ui = ActivityMainBinding.inflate(layoutInflater)
        val view = _ui.root
        setContentView(view)

        //initialize flagText view text
        showFlagNumber(MinesweeperModel.numFlags)

        val timer = object: CountDownTimer(121000, 1000) {
            override fun onTick(millis: Long) {
                val currentTime = TimeUnit.MILLISECONDS.toSeconds(millis).toInt()
                if(currentTime == 0)
                    visibilityCountdown(false)

                if(currentTime < 11){
                    _ui.tvCountdown.text = currentTime.toString()
                    _ui.tvCountdown.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.red))
                }else {
                    _ui.tvCountdown.text = currentTime.toString()
                }
            }

            override fun onFinish() {
                displaySnackbar(getString(R.string.game_over));
                MinesweeperModel.gameOver = true
            }
        }
        timer.start()

        // restart button listener
        _ui.btRestart.setOnClickListener {
            _ui.cwMinesweeper.restart()
            _ui.toggleFlag.isChecked = false
            visibilityCountdown(true)
            _ui.tvCountdown.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.black))
            timer.cancel()
            timer.start()
        }

        // state change listener
        _ui.toggleFlag.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                MinesweeperModel.mode = MinesweeperModel.FLAG
            } else {
                MinesweeperModel.mode = MinesweeperModel.TRY
            }
        }
    }

    fun showFlagNumber(numFlags: Int) {
        _ui.tvFlagText.text = getString(R.string.num_flags, numFlags.toString())
    }

    fun displaySnackbar(msg: String) {
        //make a snack bar saying "you win" or "game over"
        Snackbar.make(_ui.cwMinesweeper, msg, Snackbar.LENGTH_LONG).show()
    }

    fun visibilityCountdown(visibility: Boolean){
        if(visibility){
            _ui.tvCountdown.visibility = View.VISIBLE
        }else {
            _ui.tvCountdown.visibility = View.GONE
        }
    }
}
